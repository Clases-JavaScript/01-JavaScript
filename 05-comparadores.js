var valor = 100
var valorActualEsMenor = (valor >= 0)
var estaEnLaMitad = (valor == 50)
var casiTermina = (valor == 10)
var termina = (valor == 1)
while (valorActualEsMenor) {
    console.log(valor)

    if (estaEnLaMitad) {
        console.log(`en la mitad ${valor}`)
    }
    if (casiTermina) {
        console.log(`casi por terminar ${valor}`)
    }
    if (termina) {
        console.log(` terminar ${valor}`)
    }
    valor--
    valorActualEsMenor = (valor >= 0)
    estaEnLaMitad = (valor == 50)
    casiTermina = (valor == 10)
    termina = (valor == 1)
}
