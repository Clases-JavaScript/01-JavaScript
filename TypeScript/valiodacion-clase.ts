import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max} from "class-validator";

export class miClase {

    @Length(10, 20)
    title: string;

    @Contains("hello")
    text: string;

    @IsInt()
    @Min(0)
    @Max(10)
    rating: number;

    @IsEmail()
    email: string;

    @IsFQDN()
    site: string;

    @IsDate()
    createDate: Date;

}

let clase = new miClase();
clase.title = "Hello"; // should not pass
clase.text = "this is a great post about hell world"; // should not pass
clase.rating = 11; // should not pass
clase.email = "google.com"; // should not pass
clase.site = "googlecom"; // should not pass

validate(miClase).then(errors => { // errors is an array of validation errors
    if (errors.length > 0) {
        console.log("validation failed. errors: ", errors);
    } else {
        console.log("validation succeed");
    }
});