const nombre: string = 'kevin'
const numero: number = 3
const estado: boolean = false
const objetoPersona: object = {
    nombre: 'test'
}
// arreglos

const arregloNUmeros: Array<number> = [];
const arreglo1: number[] = [];

//mas valore en el arreglo

const arregloNUmeros1: Array<number | string> = []
const arreglo: (number | string)[] = [];


// si no se inicializa se debe poner el tipo de dato
let a: number;

// variable de cualquier tipo

let cualquierTipoDeVariable: any;


// tipado de funcion, para ver que tipo nos retorna
// para metros opcionables con "?"              
function suma(a?: number, b?: number): number {
    let a1 = 2;
    let b1 = 5
    if (a) {
        a1 = a
    }
    if (b) {
        b1 = a
    }
    return a1 + b1
};

suma(5, 5)
suma(5)
suma()

/*
se compila con el "tsc"
y se genera un "js", y se compila este
getter and setter solo para debugear o reraliza pruiebas
ejecutrar diferente version de ecmascript tsc <nombre del archivo.js> -t <version del ecmascript>
tsc 01-typescript.ts -t es2017
// parametros opcionales se usa "?"
heencia con la palabra super() en el contructor
*/

class PersonaClase {
    constructor(protected nombre?:string,protected apellido?:string){

    }
}


const personaNUeva:PersonaClase = new PersonaClase()

class hijo extends PersonaClase {
    constructor(protected nombre:string,protected apellido:string,protected apellidoMaterno:string){
        super(nombre,apellido)
        this.apellidoMaterno = apellidoMaterno
    }

}

const hijoNuevo:hijo = new hijo('no','ap','apeM')

console.log(hijoNuevo)
