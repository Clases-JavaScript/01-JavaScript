class Person {

    constructor(protected nombre?: string, protected apellido?: string, protected tipoDePersona?: string) { }

    // get getNombre():string {
    //     return this.nombre
    // }

    // set setNombre(nombre:string) {
    //     this.nombre = nombre
    // }

    // get getApellido():string {
    //     return this.apellido
    // }

    // set setApellido(apellido:string) {
    //     this.apellido = apellido
    // }

    // get setTipoDePersona():string{
    //     return this.tipoDePersona
    // }

}

class agenteDePolicia extends Person {

    constructor(protected nombre?, protected apellido?, protected tipoDePersona?, protected cargo?: string) {
        super(nombre, apellido, tipoDePersona)
        this.cargo = cargo
    }

    get getNombre(): string {
        return this.nombre
    }

    set setNombre(nombre: string) {
        this.nombre = nombre
    }

    get getApellido(): string {
        return this.apellido
    }

    set setApellido(apellido: string) {
        this.apellido = apellido
    }

    get getTipoDePersona(): string {
        return this.tipoDePersona
    }

    set setTipoDePersona(tipoDePersona: string) {
        this.tipoDePersona = tipoDePersona
    }



    get getCargo() {
        return this.cargo
    }

    set setCargo(cargo: string) {
        this.cargo = cargo
    }



}

class estudiantePoli extends Person {

    constructor(protected nombre?, protected apellido?, protected tipoDePersona?, protected facultad?: string) {
        super(nombre, apellido, tipoDePersona)
        this.facultad = facultad
    }

    get getNombre(): string {
        return this.nombre
    }

    set setNombre(nombre: string) {
        this.nombre = nombre
    }

    get getApellido(): string {
        return this.apellido
    }

    set setApellido(apellido: string) {
        this.apellido = apellido
    }

    get getTipoDePersona(): string {
        return this.tipoDePersona
    }

    set setTipoDePersona(tipoDePersona: string) {
        this.tipoDePersona = tipoDePersona
    }

    get getFacultad(): string {
        return this.facultad
    }

    set setFacultad(facultad: string) {
        this.facultad = facultad
    }


}

class empleadoSupermaxi extends Person {

    constructor(protected nombre?, protected apellido?, protected tipoDePersona?, protected areaDeTrabajo?: string) {
        super(nombre, apellido, tipoDePersona)
        this.areaDeTrabajo = areaDeTrabajo
    }

    get getNombre(): string {
        return this.nombre
    }

    set setNombre(nombre: string) {
        this.nombre = nombre
    }

    get getApellido(): string {
        return this.apellido
    }

    set setApellido(apellido: string) {
        this.apellido = apellido
    }

    get getTipoDePersona(): string {
        return this.tipoDePersona
    }

    set setTipoDePersona(tipoDePersona: string) {
        this.tipoDePersona = tipoDePersona
    }

    get getareaDeTrabajo(): string {
        return this.areaDeTrabajo
    }

    set setareaDeTrabajo(areaDeTrabajo: string) {
        this.areaDeTrabajo = areaDeTrabajo
    }

}

const personPoli: estudiantePoli = new estudiantePoli()

personaDePoli.setNombre('')
personaDePoli.setApellido('')
personaDePoli.setTipoDePersona('')
personaDePoli.setFacultad('')