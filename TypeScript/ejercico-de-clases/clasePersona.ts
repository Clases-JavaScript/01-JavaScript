class Persona {

    constructor(protected nombre?: string, protected apellido?: string, protected tipoDePersona?: string) { }

}

class PersonaPoli extends Persona {

    constructor(protected nombre?, protected apellido?, protected tipoDePersona?, protected facultad?:string) { 
        super(nombre,apellido,tipoDePersona)
        this.facultad=facultad
        }

}

class PersonaPolicia extends Persona{

    constructor(protected nombre?, protected apellido?, protected tipoDePersona?, protected cargo?:string) {
        super(nombre,apellido,tipoDePersona)
        this.cargo=cargo
     }

}

class PersonaSupermaxi extends Persona{

    constructor(protected nombre?, protected apellido?, protected tipoDePersona?, protected areaDeTrabajo?:string) {
        super(nombre,apellido,tipoDePersona)
        this.areaDeTrabajo=areaDeTrabajo
     }

}

const personaDePoli:PersonaPoli = new PersonaPoli('kevin','jimenez','hombre','sistemas')
