// http://scnakandala.blogspot.com/2013/12/clean-code-writing-code-for-humans.html

// https://zombiecodekill.com/2016/05/13/clean-code-writing-code-for-humans/

//posiciones si negativos y poositivos 
// longitudes no solo positivos

var nombre = "kevin"


var b = ''
var tn = nombre.length


while (tn >= 0) {
    b += nombre.charAt(tn)
    console.log(b)
    tn--
}

console.log(nombre.length)

console.log(nombre.indexOf('v'))

console.log(nombre.lastIndexOf('i'))

console.log(nombre.indexOf('i', 3))

/// se usa mas co expresiones regulares  regex: /in/
console.log(nombre.search('i'))

//slice  parte a la palabra y sirev con numkeros negativos y positivos
console.log(nombre.slice(2, 4))

// substring similar al slice pero se tien mas control
console.log(nombre.substr(2, 4))

// mayusculas
console.log(nombre.toUpperCase(nombre))

// minuscula
console.log(nombre.toLowerCase(nombre))

// replace reubicacion de string, solo funciona una vez
var tx = '  que fue perro'
console.log(tx.replace('fue', 'avila'))


// concatenar 
console.log(tx.concat(" ", nombre))


// trim = elimina espacio de los extremos
console.log(tx)
console.log(tx.trim())

// split si solo se deja ('') separa x caracter 
console.log(tx.split(''))

// chatAt returna la letra, ingresenado el index
console.log(tx.charAt(10))

/*
Primera palabra matuscula, inicio mitad final
separar x puntos
contar cuantos ; o , o .
espacion con *-*
*/

var noticia = 'Realiza fotografías de 20 megapíxeles y puede utilizar un perfil de color Dlog-M de 10 bits, con el que se obtiene un rango dinámico más amplio al registrar hasta mil millones de colores. Esto queda bastante por encima de los 16 millones de colores de los perfiles de 8 bits, pudiendo dejar más margen y libertad en la edición posterior del material. Por su parte, el Mavic 2 Zoom cuenta con un sensor CMOS de 1/2,3 pulgadas y zoom óptico, lo cual da nombre al propio producto. En este caso es capaz de realizar capturas de 12 megapíxeles, disponiendo un zoom automático híbrido (que combina detección de fase y contraste) de una función de "Superresolución" que recurre al zoom óptico para realizar nueve fotografías y componer una sola con todas ellas de 48 megapíxeles (con más detalle).'
var palabras = []
var letras = []
var noticiaModificada = '' 


var cambioDeLaPrimeraPalabraAMayusculas = (noticia)=>{
    palabras = noticia.split(' ')
    let primeraPalabra = palabras[0]
    let primeraPalabraMayuscula = primeraPalabra.toUpperCase(primeraPalabra) 
    noticiaModificada = noticia.replace(primeraPalabra,primeraPalabraMayuscula)
    return noticiaModificada
}

var cambioDeLaPalabraDeLaMitadAMayusculas = (noticia)=>{ 
    palabras = noticia.split(" ")
    let palabraDeLaMitad = palabras[(palabras.length/2)-0.5]
    let palabraDeLaMitadMayuscula = palabraDeLaMitad.toUpperCase(palabraDeLaMitad)
    noticiaModificada = noticia.replace(palabraDeLaMitad,palabraDeLaMitadMayuscula)
    return noticiaModificada
}

var cambioDeLaUltimaPalabraAMayusculas = (noticia)=>{ 
    palabras = noticia.split(" ")
    let ultimaPalabra = palabras[palabras.length-1]
    let ultimaPalabraMayuscula = ultimaPalabra.toUpperCase(ultimaPalabra)
    noticiaModificada = noticia.replace(ultimaPalabra,ultimaPalabraMayuscula)
    return noticiaModificada
}

cambioDeLaPrimeraPalabraAMayusculas(noticia)
cambioDeLaPalabraDeLaMitadAMayusculas(noticiaModificada)
console.log(cambioDeLaUltimaPalabraAMayusculas(noticiaModificada))


////////////////////////////////////////////////////////////////////////

var separarCuandoHayPunto = (noticia)=>{
    palabras = noticia.split('.')
    return palabras[0]
}
console.log(separarCuandoHayPunto(noticiaModificada)) 


var contasSignosDePuntacion=(noticia)=>{
    sumaComa = 0
    sumaPunto = 0
    i= 0
    letras = noticia.split('')
    while(i <= letras.length){
        if(letras[i]===','){
            sumaComa++
            
        }
        if(letras[i]==='.'){
            sumaPunto++
            
        }
        i++
    }
    
    return `suma coma: ${sumaComa}
suma punto: ${sumaPunto}`
}
console.log(contasSignosDePuntacion(noticiaModificada))

//////////////////////////////////////
var cambioDeEspaciosPorAsteriscoGuionAsterisco = (noticia)=>{
    console.log(noticia.search(' '))
    while(){
        
    }
    return noticiaModificada
}

console.log(cambioDeEspaciosPorAsteriscoGuionAsterisco(noticiaModificada))



