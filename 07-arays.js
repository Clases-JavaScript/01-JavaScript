/* var arregloNumeros = [1,2,3,4,5]

console.log(arregloNumeros.length)

//no poner numero negativos no encontrafo '-1'
console.log(arregloNumeros.indexOf())


// anadir elementos 
console.log(arregloNumeros.push(6))

//acceder al elento 
console.log(arregloNumeros[4])


// clonar arreglo, coje y los poines en otro vector
// slice y splice

// divide al vercto pero el originlal sigue igual
var arregloSlice = arregloNumeros.slice(0,3)



// eleimna los valores del arreglo original 
var arregloSplice = arregloNumeros.splice(0,2)

// metodo pop eliminaa el siempre el ultimo elemrnto del arreglo

console.log(arregloNumeros.pop())


// recorrido de arreglos
// mediante operadores

arregloNumeros.forEach(function(valor,index,arreglo){
    console.log('valor: '+valor)
    console.log('index: '+index)
    console.log('arreglo: '+arreglo)
})

// map:  entrega otro arreglo con cambios que se realizo a el
var operadorMap = arregloNumeros.map((valor,index,arreglo)=>{
    if(index === 2){
         valor += 7
    }
return valor
})

console.log(arregloNumeros)

console.log(operadorMap)


// filter; enrega un vector con el valor deseado o encontrado
var OperadorFilter = arregloNumeros.filter((valor,index)=>{
    return valor === 3
}) 
console.log(OperadorFilter)

// find: bussca un numero, lo que retorna es solo un numero y no un vector

var operadorFindr = arregloNumeros.find((valor,index)=>{
    return valor === 13
}) 
console.log(operadorFindr)

// every y some : verificar el vector on si tuviera una condicion
var arregloNumber = [2,4,5,8,6,9,10]
// every: todos deben cumplir una condicion


var funcionEvery = arregloNumber.every((valor,index)=>{
    var esMismoTipo = typeof valor === 'number'
    if(esMismoTipo){
        console.log(valor)
        console.log(index)
        return true
    }
    return false
})

console.log(funcionEvery)

// some : busca el primero que cumpla la condicion
var funcionSome = arregloNumber.some(valor=>{
    return typeof valor === 'number'
})

console.log(funcionSome) */
var arregloNumber = [2, 4, 5, 8, 6, 9, 10]
var suma = 0
arregloNumber.forEach(function (valor, index, arreglo) {
    suma += valor
})

console.log(suma)

// reduce: nos permite operar con todos los elementos y nos retorna un valor
// parametros diferentes : una funciojn y un acumulador
// dentro de la funccion tbm recibe el caumulador inicializado y el valor
// var resultadoConReduce = arregloNumber.reduce((acumulador,valor,index)=>{
//     if(index%2 === 0){
//         acumulador+=valor
//     }
//     return acumulador
// },0)

// console.log(resultadoConReduce)


//************************************************ */

var usuarios = [
    {
        nombre: 'juan',
        sexo: 'M',
        nombreMascota: 'firulais'
    },
    {
        nombre: 'edwin',
        sexo: 'F',
        nombreMascota: 'fabricioPerro'
    },
    {
        nombre: 'jakiro',
        sexo: 'M',
        nombreMascota: 'jack'
    },
    {
        nombre: 'Patricia',
        sexo: 'F',
        nombreMascota: 'cachetes'
    }
]
// var respuesta = []
// var ejemplonReduce = usuarios.reduce((acumulador,valor)=>{

//     if(valor.sexo === 'M'){
//         acumulador.push(valor)        
//         //acumulador += valor
//     }
//     return acumulador
// },respuesta)


// console.log(ejemplonReduce)

var sumaEdades = (a,b)=>{
    return a+b
}

var anadirEdades = usuarios.map((valor) => {
    valor.edad = Math.floor(Math.random()*100)
    valor.edadMascota = Math.floor(Math.random()*10)
    valor.sumaEdades = sumaEdades(valor.edad,valor.edadMascota)
    return valor
})
.reduce((acumulador,valor)=>{
    let esMasculinoYMayorA20 = (valor.sexo === 'M' && valor.edad <= 20)
    if(esMasculinoYMayorA20){
        acumulador.push(valor)  
    }
    return acumulador
},[ ])

console.log(anadirEdades)