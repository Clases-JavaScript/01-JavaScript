json = {
    nombre: 'kevin',
    apellido: 'jimenez',
    edad: 22
}

json2 = {
    nombre: 'kathy',
    apellido: 'jimenez',
    edad: 16
}


json3 = {
    nombre: 'orlando',
    apellido: 'saraguro',
    edad: 22,
    hermana: [
        {
            nombre: 'ariel',
            apellido: 'lopez',
            edad: 16
        }
    ],
    padres:[
        {
            nombre: 'Orlando',
            apellido: 'Maza',
            edad: 56
        },
        {
            nombre: 'Marlene',
            apellido: 'Soto',
            edad: 44
        }
    ]

}

json4 = {
    nombre: 'paulia',
    apellido: 'Iguana',
    edad: 16,
    amigos:[
        {
            nombre: 'gina',
            apellido: 'bustamante',
            edad: 56
        },
        {
            nombre: 'mairia',
            apellido: 'jaramillo',
            edad: 44
        },
        {
            primas:[
                {
                    nombre: 'Orlando',
                    apellido: 'Maza',
                    edad: 56
                },
                {
                    nombre: 'Marlene',
                    apellido: 'Soto',
                    edad: 44
                }
            ]
        }
        

    ] 
}

var copiaJson4 = JSON.parse(JSON.stringify(json4));



console.log(copiaJson4)

copiaJson4.conocidos = [
    {
        nombre: 'juan',
        apellido: 'saraguro',
        edad: 28
    },
    {
        nombre: 'melany',
        apellido: 'safla',
        edad: 28
    }

]

console.log(copiaJson4)


////////////////////////////////////////////


////////////////////
operaconEntreApellidos1 = `sdfsd`+json3.padres[1].apellido + json4.amigos[2].primas[0].apellido
operaconEntreApellidos2 = json.apellido + json4.amigos[1].apellido
operaconEntreApellidos3 = copiaJson4.conocidos[1].apellido + json2.apellido

///////////////// suma
operaconEntreEdades1 = json3.padres[1].edad + json4.amigos[2].primas[0].edad
operaconEntreEdades2 = json.edad + json3.padres[0].edad
operaconEntreEdades3 = copiaJson4.conocidos[0].edad + json2.edad

//////// resta
operaconEntreEdades1 = json3.padres[1].edad + json4.amigos[2].primas[0].edad
operaconEntreEdades2 = json.edad + json3.padres[0].edad
operaconEntreEdades3 = copiaJson4.conocidos[0].edad + json2.edad


///////////////// multiplicacion

operaconEntreEdades1 = json3.padres[1].edad + json4.amigos[2].primas[0].edad
operaconEntreEdades2 = json.edad + json3.padres[0].edad
operaconEntreEdades3 = copiaJson4.conocidos[0].edad + json2.edad

////////////////////////////// division

operaconEntreEdades1 = json3.padres[1].edad + json4.amigos[2].primas[0].edad
operaconEntreEdades2 = json.edad + json3.padres[0].edad
operaconEntreEdades3 = copiaJson4.conocidos[0].edad + json2.edad




//////////////////////////////// mensajes

///////////// opcion 1
var mensajes =  'apellido del primer json '+ json.apellido + ' apellido del segundo json ' + json2.apellido
console.log(mensajes)

 ////////// opcion 2

var msj2 =`apellido del primer jason de la primer forma de imprimir ${json.apellido} ` + ` apellido del primer jason de la segunda forma de imprimir ${json2.apellido}`
console.log(msj2)


console.log(operaconEntreApellidos1)
console.log(operaconEntreApellidos2)
console.log(operaconEntreEdades1)
console.log(operaconEntreEdades2)
console.log(operaconEntreApellidos3)
console.log(operaconEntreEdades3)

