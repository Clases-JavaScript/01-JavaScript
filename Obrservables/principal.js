const funciones = require('./paqueteDeFunciones')
const { Observable, Subject, ReplaySubject, from, of, range, throwError } = require('rxjs');
const { switchMap, map, filter, distinct, mergeMap, catchError } = require('rxjs/operators');


respuestaDelObservable = (respuesta) => {
    console.log(respuesta)
}
ErrorDelObservable = (error) => {
    console.log(error)
}

terminoElObservable = () => {
    console.log('termino')
}


const data = {
    arreglo: funciones.datosDePrueba.misUsuarios,
    usuario: funciones.datosDePrueba.usuarioABuscarOCrear
}



const datos$ = of(data)

const observableCrearUsuario = (datos) => {
    
    const promesaDeCrearUsuario = from(funciones.crearUsuario(datos.arreglo, datos.usuario))
    return promesaDeCrearUsuario

}

datos$
.pipe(
    mergeMap(observableCrearUsuario)
)
.subscribe(
    respuestaDelObservable,
    ErrorDelObservable,
    terminoElObservable
)

