// innstalar, ayuda a js a ser vas facil
// se usa en varios leguajes
const { Observable, Subject, ReplaySubject, from, of, range, throwError } = require('rxjs');
const { switchMap, map, filter, distinct, mergeMap, catchError } = require('rxjs/operators');

respuestaDelObservable = (respuesta)=>{
    console.log(respuesta)
}
ErrorDelObservable = (error)=>{
    console.log(error)
}

terminoElObservable = ()=>{
    console.log('termino')
}

// para poder usar se usa el "of()" lo cual contine una cadena de datos
var observableEjemplo$ = of(1,2,3,4,4,5,6,7)

// para obterner la respuest del observable se usa el subscribe, recibe 3 callback, respuest, error, y termino observable
/*observableEjemplo$
.subscribe(
    respuestaDelObservable,
    ErrorDelObservable,
    terminoElObservable)*/

//console.log(observableEjemplo$)


// cocatenar funciones con el pipe y se concatena con la coma
// se usa con los operadores
function sumarMas1(numero){
    return numero+1
}

function multiplicarx2(numero){
    const numero2 = numero
    //throw({msj: 'hola'})
    return numero*2
}
const promiseResta = (numero)=>{
    const funcionPromesa=(resolve,reject)=>{
        resolve(numero-1)
    }
    return new Promise(funcionPromesa)
}
// para pasar de promise a observable se usa "from()"
const observableResta = (numero)=>{
    const promesaObservableConvertido$ = from(promiseResta(numero))
    return promesaObservableConvertido$;
}

observableEjemplo$
.pipe(
    map(sumarMas1),
    map(multiplicarx2),
    distinct(),
    mergeMap(observableResta))
.subscribe(
    respuestaDelObservable,
    ErrorDelObservable,
    terminoElObservable)