const buscarUsuario = require('./buscarUsuario.js')
const crearUsuario = require('./crearUsuario.js')
const eliminarUsuario = require('./eliminarUsuario.js')
const datosDePrueba = require('./datosDePrueba.js')
const buscarDiferentesUsuarios = require('./buscarDiferentesUsuarios.js')
const buscarCrearUsuario = require('./buscarYCrearUsuario.js')


module.exports = {
    datosDePrueba,
    crearUsuario,
    buscarUsuario,
    eliminarUsuario,
    buscarDiferentesUsuarios,
    buscarCrearUsuario
}

