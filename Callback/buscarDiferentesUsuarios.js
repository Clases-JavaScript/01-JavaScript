module.exports = (arreglo,parametroDeBusqueda,cb)=>{
    let arregloDeUsuariosEncontrados = arreglo.filter(usuarios=>{    
        const usuarioEncontrado = usuarios.nombre.search(parametroDeBusqueda) !=-1 
        if (usuarioEncontrado) {        
            return usuarios
        }
    })
    
    if (arregloDeUsuariosEncontrados) {
        cb({
            mensaje: 'se encontro usuarios',
            arregloDeUsuariosEncontrados
        })
    } else {
        cb({
            mensaje: 'no se encontro ningun usuarios'
        })    
    }
}