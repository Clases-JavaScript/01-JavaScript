import { COLORE_SEMAFORO } from "../TypeScript/enums";

interface usuarioInterfaces {
    nombre: string,
    apellido: string
}

const usuarioConInterfaz: usuarioInterfaces = {
    nombre: 'HOLA',
    apellido: 'QUE'
}

/// implemete nos ayuda  a implemtar las n¡interfaces ne una clase
/**
 * para poder implementar mas se concatena con "," // metodos abtractos
 * conla herencias no se ude hacer mas de una
 * 
 * 
 * en las interfaces se puede heredar mas de uno
 */

interface correo{
    correo:string;
    enviarCorreo():boolean
}

interface tarbajo{
    direccion:string;
    dondeTrabaja():boolean
}

interface todoUno extends usuarioInterfaces,correo,tarbajo{
    vale:boolean
}

class usuarioVacio implements todoUno{
    vale: boolean;    nombre: string;
    apellido: string;
    correo: string;
    enviarCorreo(): boolean {
        throw new Error("Method not implemented.");
    }
    direccion: string;
    dondeTrabaja(): boolean {
        throw new Error("Method not implemented.");
    }
}

const colorSemaforoParar = COLORE_SEMAFORO.rojo;