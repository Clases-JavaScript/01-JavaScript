

module.exports = (arreglo,usuarioACrear)=>{ 
    const funcionPromiseDeCrearUsuario = (resolve,reject)=>{        
        if (usuarioACrear) {
            arreglo.push(usuarioACrear)
            resolve({
                mensaje: 'usuario creado exitosamente',
                usuarios: arreglo
            })    
        } else {
            reject({
                mensaje: 'usuario no creado',
                usuarioACrear            
            })    
        }
        
        
    }              
    return new Promise(funcionPromiseDeCrearUsuario)    
}