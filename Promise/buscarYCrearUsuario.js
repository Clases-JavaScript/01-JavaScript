const paquetes = require('./paquetes.js')

module.exports = (arreglo,usuarioAbuscarOCrear)=>{    

    // console.log(arreglo)

    // console.log('////')

    // console.log(usuarioAbuscarOCrear)

    const promiseDeBuscarYCrear = (resolve,reject)=>{
        paquetes.buscarUsuario(arreglo,usuarioAbuscarOCrear)
        .then(respuestaDeLaPromiseBuscarUsuario=>{
            resolve({
                respuestaDeLaPromiseBuscarUsuario
            })
            
        })
        .catch(respuestaDeLaPromiseBuscarUsuario=>{    
            return paquetes.crearUsuario(arreglo,usuarioAbuscarOCrear)
        })
        .then(r=>{
            resolve({
                mensaje: r.mensaje,
                usuarios: r.usuarios
            })
        }).catch(r=>{
            
        })

        

    }
    return new Promise(promiseDeBuscarYCrear)
    
}