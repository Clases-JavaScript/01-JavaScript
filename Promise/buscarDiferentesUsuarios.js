
module.exports = (arreglo,parametroDeBusqueda)=>{
    let arregloDeUsuariosEncontrados = arreglo.filter(usuarios=>{    
        const usuarioEncontrado = usuarios.nombre.search(parametroDeBusqueda) !=-1 
        if (usuarioEncontrado) {        
            return usuarios
        }
    })

    const funcionPromiseDeEliminarUsuario = (resolve,reject)=>{
        if (arregloDeUsuariosEncontrados) {
            resolve({
                mensaje: 'se encontro usuarios',
                arregloDeUsuariosEncontrados
            })
        } else {
            reject({
                mensaje: 'no se encontro ningun usuarios'
            })    
        }
    }


    return new Promise(funcionPromiseDeEliminarUsuario)
}