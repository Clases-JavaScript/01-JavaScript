var json = {
    nombre: 'kevin',
    edad: 23,
    soltero: true,
    fecha: new Date(1955-02-24)
    }
    


console.log(json.nombre)
console.log(json["edad"])



json.nombre = "nuevo"

json.direccion = "direccio"

console.log(json)

delete json.direccion

console.log(json)

////////////// mi json
console.log(b)
console.log(json)
console.log()
console.log()
console.log()
console.log('////////////////// JSON')
 
var miJson = {
    nombres: 'kevin orlando',
    apellidos: 'jimenez saraguro',
    edad: 23,
    fechaDeNacimiento: new Date('1955-02-24'),
    intereses: {
        hobies: [
            'jugar play',
            'jugar futbol',
            'ver series',
            'leer libros',
            'dibujar'
        ],
        peliculas:[
            {
                nombre: 'batman',
                tipo: 'ficcion',
                esBuena: true
            },
            {
                nombre: 'justice league',
                tipo: 'ficcion',
                esBuena: true
            },
            {
                nombre: 'gol',
                tipo: 'motivaciob',
                esBuena: true
            },
            {
                nombre: 'ese no es mi hijo',
                tipo: 'comedia',
                esBuena: true
            },
    
        ],
        deportes:[
            {
                nombre: 'futbol',
                meGusta: true,
            },
            {
                nombre: 'volley',
                meGusta: true,
            },
            {
                nombre: 'basketball',
                meGusta: true,
            },
            {
                nombre: 'MUY TAI',
                meGusta: true,
            },
        ],

    },
    familiares:[
        {
            nombres: 'Maximo Orlando',
            apellidos: 'jimenez maza',
            edad: 56,
            fechaDeNacimiento: new Date('1976-06-17'),
        },
        {
            nombres: 'marlene de jesus',
            apellidos: 'saraguro soto',
            edad: 40,
            fechaDeNacimiento: new Date('1980-02-24'),
        },
        {
            nombres: 'katherine paulina',
            apellidos: 'jimenez saraguro',
            edad: 16,
            fechaDeNacimiento: new Date('2002-02-18'),
        },
    ]

}

console.log(miJson)


// transforma el json a un json-string
var objetoStringJson = JSON.stringify(json)

// despues se puede clonar
var b = JSON.parse(JSON.stringify(json))

b.nombre = 'kevin'

console.log(b)
console.log(json)
console.log()
console.log()
console.log()
console.log('////////////////// operaciones')

var a = null
var b = 'd'

var operacion = json+miJson

console.log(json+miJson)

console.log(typeof operacion)