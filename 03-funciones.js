

/**
 *funcion saluda
 *
 * @param {string} nombre hola
 * 
 */
function saludar(nombre){
    return 'hola'
}

saludar()
console.log(saludar())




//////////////// funciones anonimas

var funcionAnomina = function(){
    return 'funcion anonima'
}


////////////////// funcion arrow flat

var funcionArrow = ()=>{
    return 'funcion flecha'
}


console.log(funcionAnomina())
console.log(funcionArrow())




///////////// funcion con parametros

var operacion = (a,b)=>{
    return a + b
}


operacion(4,'k');
console.log(operacion(4,'k'))


///////////////////// funciones en json

var json = {
    sumar: ()=>{
        return 'funcion flecha'
    }

}


///////////variables

/* var ///global

let /// local

const // constanbte */